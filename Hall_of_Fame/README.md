[![banner](others/images/ml2.png)](https://www.its.kit.edu/Lehrveranstaltungen_DataDrivenEngineering_I.php)

# Best Project Contest: Hall of Fame

At the end of each lecture period, we are organizing best project contest, $`(KI)^2T`$, during which the finalists present their work. The best is decided by the votes of the audience! 

We will update the list every semester. We thank our finalists for their excellent work and sharing their project files with us!

## DDE I: Machine Learning for Dynamical Systems Winter Semester 2022

| Participant | Project Title | Rank |
| :------ | :------ | :------: |
| Flavia Gehrig | [How to Manage the Bike Sharing Demand for Sustainable Cities](/Hall_of_Fame/DDE1/Gehrig_Flavia_bike_sharing_demand.ipynb) | 1st |
| Thomas Zink | [The Right to Breathe Clean Air: A Closer look to Seoul](/Hall_of_Fame/DDE1/Zink_Thomas_AirQualityForecasting.ipynb) | 2nd |
| Dominik Fletschinger | [Energy Consumption Predictions at High Granularity](/Hall_of_Fame/DDE1/Fletschinger_Dominik_Hourly_Energy_Consumption_US.zip) | 3rd |
| Benjamin Trung-Hieu Le| [Feature-Enriched Short Term Electricity Price Forecasting](/Hall_of_Fame/DDE1/Le_Trung-Hieu_Benjamin_Hourly_Energy_Demand.rar) | 3rd |
| Juan José Zapata| [ML-Augmented Condition Monitoring of Hydraulic Systems](/Hall_of_Fame/DDE1/Zapata_Juan_Condition_Monitoring_of_Hydraulic_Systems.ipynb)|Top V |


## DDE I: Machine Learning for Dynamical Systems Winter Semester 2021

| Participant | Project Title | Rank |
| :------ | :------ | :------: |
| Marius Steger | [MRI and Alzheimers](/Hall_of_Fame/DDE1/MRI_Alzheimers.ipynb) | 1st |
| Moritz Kopp | [Early stage diabetes risk prediction](/Hall_of_Fame/DDE1/Diabetes_risk.ipynb) | 2nd |
| Joel Arweiler | [Temperature forecasting](/Hall_of_Fame/DDE1/Temperature_forecasting.ipynb) | 3rd |
| Tim Scheurenbrand| [Stock market analysis](/Hall_of_Fame/DDE1/Stock_market_analysis.ipynb) |Top V |
| Matthias Vogt | [Wind power forecasting ](/Hall_of_Fame/DDE1/Wind_Power_Forecasting.ipynb)|Top V |
| Marian Wilhelm | [MotionSense](/Hall_of_Fame/DDE1/MotionSense.ipynb) |Top V |

> Note that Top V finalists are given in alphabetical order.


## License
All the materials presented in this repository is copyright 2021 [Data Driven Engineering Lecture Series](https://www.its.kit.edu/Lehrveranstaltungen_DataDrivenEngineering_I.php). All Rights Reserved.

Licensed under the ITS-KIT License. You may not use this file except in compliance with the License. If you use the material presented in this repository and/or modified versions of the materials outside DDE lectures of ITS must reference:

> © ITS-KIT DDE: Data Driven Engineering Lecture Series
>
> http://www.its.kit.edu
